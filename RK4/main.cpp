#include <iostream>
#include <cmath>
#include <cstdio>
#include "rk4.hpp"

using namespace std;

/***
 * author: Cory Norris
 * credit: http://gafferongames.com/game-physics/integration-basics/
 *
 * Learning Goals
 * 1. Switch from integrating velocity directly from acceleration
 *  to integrating momentum from force instead (the derivative of
 *  momentum is force). You will need to add “mass” and “inverseMass”
 *  to the State struct and I recommend adding a method called
 *  “recalculate” which updates velocity = momentum * inverseMass
 *  whenever it is called. Every time you modify the momentum value
 *  you need to recalculate the velocity. You should also rename
 *  the acceleration method to “force”. Why do this? Later on when
 *  working with rotational dynamics you will need to work with
 *  angular momentum directly instead of angular velocity, so you
 *  might as well start now.
 *
 * 2. Try modifying the integrate method to implement an Euler
 *  integrator. Compare the results of the simulation against the
 *  RK4 integrator. How much can you increase the spring constant
 *  k before the simulation explodes with Euler? How large can you
 *  make it with RK4?
 *
 * 3. Extend position, velocity and force to 3D quantities using
 *  vectors. If you use your intuition you should easily be able
 *  to extend the RK4 integrator to do this.
 *
 ***/



void display(State state){
    cout<<"-- State --"<<endl;
    cout<<"state.v: "<<state.v<<endl;
    cout<<"state.x: "<<state.x<<endl;
}

int main()
{
     RK4 testRK4;
    /*
    cout << "Testing RK4" << endl;

    State testState;
    testState.x = 0;
    testState.v = 0;
    display(testState);
    //for (int t = 0; t < 10; t++){
    //    testRK4.integrate(testState,t,1);

    testRK4.integrate(testState, 0, 10);
    display(testState);
    //}
    */

    // Start our spring with a velocity of 0, but a position of 100 (eg, pulled back).
    State state;
    state.x = 100;
    state.v = 0;

    float t = 0;
    float dt = 0.1f;

    while (abs(state.x)>0.001f || abs(state.v)>0.001f)
    {
        printf("%.2f, %.2f\n", state.x, state.v);
        testRK4.integrate(state, t, dt);
        t += dt;
    }

    getc(stdin);


    return 0;
}

