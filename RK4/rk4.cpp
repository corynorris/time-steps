#include "rk4.hpp"

RK4::RK4()
{
}

Derivative RK4::evaluate(const State &initial, float t, float dt, const Derivative &d)
{
    //Advance the current state ahead dt seconds, using euler integration
    State state;
    state.x = initial.x + d.dx*dt;
    state.v = initial.v + d.dv*dt;


    //Calculate the new derivatives using the integrated state.
    Derivative output;
    output.dx = state.v;
    output.dv = acceleration(state,t+dt);
    return output;
}

Derivative RK4::evaluate(const State &initial, float t)
{
    Derivative output;
    output.dx = initial.v;
    output.dv = acceleration(initial, t);
    return output;
}

void RK4::integrate(State &state, float t, float dt)
{
     Derivative a = evaluate(state, t);
     Derivative b = evaluate(state, t, dt*0.5f, a);
     Derivative c = evaluate(state, t, dt*0.5f, b);
     Derivative d = evaluate(state, t, dt, c);

     const float dxdt = 1.0f/6.0f * (a.dx + 2.0f*(b.dx + c.dx) + d.dx);
     const float dvdt = 1.0f/6.0f * (a.dv + 2.0f*(b.dv + c.dv) + d.dv);

     state.x = state.x + dxdt * dt;
     state.v = state.v + dvdt * dt;
}

float RK4::acceleration(const State &state, float t)
{
    // Just arbitrarily defined
    const float k = 10;
    const float b = 1;

    // Calculates a spring and damper force and returns it as the acceleration assuming unit mass.
    return - k*state.x - b*state.v;

    // return 10;
}
