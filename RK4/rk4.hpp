#ifndef RK4_HPP
#define RK4_HPP

// Adapted from site:
// http://gafferongames.com/game-physics/integration-basics/

struct State {
    float x;    //position
    float v;    //velocity
};

struct Derivative {
    float dx;   //derivative of position (velocity)
    float dv;   //derivative of velocity (acceleration)
};


class RK4
{
public:
    RK4();
    void integrate(State & state, float t, float dt);
private:
    /**
     * @brief evaluate: advances the physics state from d to t+dt
     * @param initial: The current state of the object.
     * @param t:
     * @param dt: The time difference to advance
     * @param d: Passed in derivatives to calculate euler integration
     * @return
     */
    Derivative evaluate(const State &initial, float t, float dt, const Derivative &d);
    Derivative evaluate(const State &initial, float t);

    float acceleration(const State &state, float t);
};

#endif // RK4_HPP
