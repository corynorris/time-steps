#include <iostream>
#include "../../RK4/rk4.hpp"

using namespace std;

/***
 * author: Cory Norris
 * credit: http://gafferongames.com/game-physics/fix-your-timestep/
 *
 *
 * From: http://gamedev.stackexchange.com/questions/1589/fixed-time-step-vs-variable-time-step
 * Should Physics step rate be tied to your frame rate? No.
 * Should physics be stepped with constant deltas? Yes.
 *
 */


void render(State state)
{
    cout<<"x: "<<state.x<<endl;
    cout<<"v: "<<state.v<<endl;
}

int main()
{
    RK4 testRK4;


    /***
     * Fixed Delta Time
     * ---
     * Would only work if delta time of your physics matched that of
     * your display rate, and you could ensure that your update loop
     * takes less than 1 frame.
     *
     * In otherwords, dt would have to be the same as the refresh rate
     * of the monitor, and all the calculations could fit in 1 round.
     * Essentially, this has the obvious problem of not being regulated
     * at all.
     */

    cout<< "Fixed Delta Time" <<endl;
    double t = 0.0;
    double dt = 1.0 / 60.0;

    while ( !quit )
    {
      testRK4.integrate( state, t, dt );
      render( state );
      t += dt;
    }


    /***
     * Variable Delta Time
     * ---
     * Here, we measure the time the previous step took and use the
     * time difference in our updates.  However, this means our
     * simulation must account for ANY value of delta time, which is
     * near impossible, particularly given the inprecision of floats.
     */

    double t = 0.0;

    double currentTime = hires_time_in_seconds();

    while ( !quit )
    {
         double newTime = hires_time_in_seconds();
         double frameTime = newTime - currentTime;
         currentTime = newTime;

         testRK4.integrate( state, t, frameTime );
         t += frameTime;

         render( state );
    }



    /***
     * Semi-fixed Timestep
     * ---
     * In order to limit the range of delta time, we can implement a
     * 'semi-fixed' time step.  That is to say, step through the delta
     * time in chunks of a predetermined maximum size, until the delta
     * time is less than one of those chunks.  We're now however,
     * taking multiple steps per render.  Additionally, if we start to
     * fall behind, we'll continue to fall further and further behind.
     */


    double t = 0.0;
    double dt = 1 / 60.0;

    double currentTime = hires_time_in_seconds();

    while ( !quit )
    {
         double newTime = hires_time_in_seconds();
         double frameTime = newTime - currentTime;
         currentTime = newTime;

         while ( frameTime > 0.0 )
         {
             const float deltaTime = min( frameTime, dt );
             testRK4.integrate( state, t, deltaTime );
             frameTime -= deltaTime;
             t += deltaTime;
         }

         render( state );
    }




    /***
     * Free the physics
     * ---
     * Though semi-fixed is *usually* good enough, it is not necessarily
     * identical between runs (because in floating point arithmatic,
     * v*dt + v*dt does not necesarily equal 2(v*dt)).  By using an
     * accumulator, we can guarantee that we're ALWAYS stepping by a
     * fixed DT.  In semi-fixed, we were stepping min(dt, frametime).
     *
     * Another way to look at things is as follows: The renderer produces
     * time and the simulation consumes it in discrete dt sized chunks.
     */

    double t = 0.0;
    const double dt = 0.01;

    double currentTime = hires_time_in_seconds();
    double accumulator = 0.0;

    while ( !quit )
    {
         double newTime = hires_time_in_seconds();
         double frameTime = newTime - currentTime;
         currentTime = newTime;

         accumulator += frameTime;

         while ( accumulator >= dt )
         {
              integrate( state, t, dt );
              accumulator -= dt;
              t += dt;
         }

         render(state);
    }


    /***
     * Touch-up
     * ---
     * This is quite good, however it suffers from an effect called
     * "temporal aliasing".  This is the effect of showing a frame that
     * is slightly off from the physics frame, for example, if our physics
     * rate runs at 35 fps, and our display rate is 60.  We'll be showing
     * at 35 (25 seconds too early), followed by 105 (15 seconds too early)
     * etc, which is visually unpleasent.
     *
     * To solve this, we do what's called "interpolation".  This way, we
     * can render a scene that is consistent with where we are in our
     * time step.
     *
     */

    double t = 0.0;
       const double dt = 0.01;

       double currentTime = hires_time_in_seconds();
       double accumulator = 0.0;

       State previous;
       State current;

       while ( !quit )
       {
            double newTime = time();
            double frameTime = newTime - currentTime;
            if ( frameTime > 0.25 )
                 frameTime = 0.25;	  // note: max frame time to avoid spiral of death
            currentTime = newTime;

            accumulator += frameTime;

            while ( accumulator >= dt )
            {
                 previousState = currentState;
                 integrate( currentState, t, dt );
                 t += dt;
                 accumulator -= dt;
            }

            const double alpha = accumulator / dt;

            State state = currentState*alpha + previousState * ( 1.0 - alpha );

            render( state );
       }

    return 0;
}
